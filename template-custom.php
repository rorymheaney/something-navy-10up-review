<?php
/**
 * Template Name: About
 */
?>

<?php
	$imageOne = get_field('image_header');
	$imageTwo = get_field('image_right_p1');
	$imageThree = get_field('image_bottom');
	$imageFour = get_field('image_top_p3');
	$askImage = get_field('ask_image');
?>

<div class="about-blocks clearfix">
	<div class="about-blocks__header-image">
		<img src="<?php echo $imageOne['url'];?>" alt="header image">
	</div>
	<div class="about-blocks__intro">

		<div class="about-blocks__intro-blocks about-blocks__intro-blocks--left">
			<h1 class="about-blocks__intro-title">
				<?php the_field('title_intro');?>
			</h1>

			<?php the_field('content_left_intro');?>
		</div>
		<div class="about-blocks__intro-blocks about-blocks__intro-blocks--right">
			<?php the_field('content_right_intro');?>
		</div>

	</div>

	<!-- p1 -->
	<div class="about-blocks__p1">
		<div class="about-blocks__p1-block about-blocks__p1-block--left">
			<h1 class="about-blocks__p1-title">
				<?php the_field('title_about');?>
			</h1>
			<?php the_field('content_left_p1');?>
		

			<span class="about-blocks__p1-tag-line">
				<?php the_field('tag_line');?>
			</span>
		</div>

		

		<img class="about-blocks__p1-img"  src="<?php echo $imageTwo['url'];?>" alt="image right"/>
	</div>

	<!-- p2 -->
	<div class="about-blocks__p2">
		<div class="about-blocks__p2-block about-blocks__p2-block--left">
			<h1 class="about-blocks__p2-title">
				<?php the_field('title_p2');?>
			</h1>
			<?php the_field('content_top');?>

			<img class="about-blocks__p2-img"  src="<?php echo $imageThree['url'];?>" alt="image right"/>
		</div>

		<div class="about-blocks__p2-block about-blocks__p2-block--right">
			<img class="about-blocks__p3-img"  src="<?php echo $imageFour['url'];?>"  alt="image right"/>

			<h1 class="about-blocks__p2-title">
				<?php the_field('title_p3');?>
			</h1>
			<?php the_field('content_bottom_3');?>

			<div class="about-blocks__misc">
				<?php the_field('misc');?>
			</div>

						
		</div>
	</div>

	<!-- ask -->
	<div class="about-blocks__ask">
		<div class="about-blocks__ask-img-con">
			<img class="about-blocks__ask-img"  src="<?php echo $askImage['url'];?>" alt="image right"/>
		</div>

		<div class="about-blocks__p1-block about-blocks__p1-block--left">
			<?php the_field('content_left_ask');?>
		</div>
		<div class="about-blocks__p1-block about-blocks__p1-block--right">
			<?php the_field('content_right_ask');?>
		</div>



      <!-- comments -->
<?php comments_template('/templates/comments.php'); ?>
	</div>
	<!-- ask end -->

</div>


