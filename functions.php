<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/customizer.php' // Theme customizer
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }

  require_once $filepath;
}
unset($file, $filepath);


// acf image view size
add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
    .acf-image-uploader img {
      max-width:150px;
    } 
  </style>';
}





//custom gallery
/**
 * Set up the new field in the media module.
 *
 * @return void
 */
function additional_gallery_settings() {
  ?>

    <script type="text/html" id="tmpl-custom-gallery-setting">
        <span>Style</span>
        <select data-setting="style">
            <option value="default-style">Default Style</option>
            <option value="fifty-fifty">50/50 Style</option>
        </select>
    </script>

    <script type="text/javascript">
        jQuery( document ).ready( function() {
            _.extend( wp.media.gallery.defaults, {
                style: 'default-style'
            } );

            wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend( {
                template: function( view ) {
                    return wp.media.template( 'gallery-settings' )( view )
                         + wp.media.template( 'custom-gallery-setting' )( view );
                }
            } );
        } );
    </script>

  <?php
}
add_action( 'print_media_templates', 'additional_gallery_settings' );


//custom slider for gallery
add_filter('post_gallery', 'my_post_gallery', 10, 2);
function my_post_gallery($output, $attr) {
    global $post;

    if (isset($attr['orderby'])) {
        $attr['orderby'] = sanitize_sql_orderby($attr['orderby']);
        if (!$attr['orderby'])
            unset($attr['orderby']);
    }

    extract(shortcode_atts(array(
        'order' => 'ASC',
        'orderby' => 'menu_order ID',
        'id' => $post->ID,
        'itemtag' => 'dl',
        'icontag' => 'dt',
        'captiontag' => 'dd',
        'columns' => 3,
        'size' => 'thumbnail',
        'include' => '',
        'exclude' => ''
    ), $attr));

    $id = intval($id);
    if ('RAND' == $order) $orderby = 'none';

    if (!empty($include)) {
        $include = preg_replace('/[^0-9,]+/', '', $include);
        $_attachments = get_posts(array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby));

        $attachments = array();
        foreach ($_attachments as $key => $val) {
            $attachments[$val->ID] = $_attachments[$key];
        }
    }

    if (empty($attachments)) return '';

    // Here's your actual output, you may customize it to your need
    $output .= "<div class=\"gallery-output clearfix\">\n";
    //$output .= "<div class=\"preloader\"></div>\n";
    $output .= "<ul class=\"gallery-output__list\">\n";

    // Now you loop through each attachment
    foreach ($attachments as $id => $attachment) {
        // Fetch the thumbnail (or full image, it's up to you)
//      $img = wp_get_attachment_image_src($id, 'medium');
//        $imgThumbnail = wp_get_attachment_image_src($id, 'thumbnail');
        $img = wp_get_attachment_image_src($id, 'full');

        $output .= "<li class=\"gallery-output__item\" data-img=\"{$img[0]}\">\n";
        $output .= "<div class=\"gallery-output__bg\" style=\"background-image: url({$img[0]});\">\n";
        $output .= "</div>\n";
        $output .= "</li>\n";
    }

    

    $output .= "</ul>\n";
    $output .= "</div>\n";

    return $output; 

}

/**
 * HTML Wrapper - Support for a custom class attribute in the native gallery shortcode
 *
 * @param string $html
 * @param array $attr
 * @param int $instance
 *
 * @return $html
 */
function customize_gallery_abit( $html, $attr, $instance ) {

    if( isset( $attr['style'] ) && $style = $attr['style'] ) {
        // Unset attribute to avoid infinite recursive loops
        unset( $attr['style'] ); 

        // Our custom HTML wrapper
        $html = sprintf( 
            '<div class="gallery-container wpse-gallery-wrapper-%s">%s</div>',
            esc_attr( $style ),
            gallery_shortcode( $attr )
        );
    }

    return $html;
}
add_filter( 'post_gallery', 'customize_gallery_abit', 10, 3 );


/* Add Placehoder in comment Form Fields */
  
add_filter( 'comment_form_default_fields', 'digitalsky_comment_placeholders' );
function digitalsky_comment_placeholders( $fields )
{
    $fields['author'] = str_replace(
        '<input',
        '<input placeholder="Name"',
        $fields['author']
    );
    $fields['email'] = str_replace(
        '<input',
        '<input placeholder="Email"',
        $fields['email']
    );
    $fields['url'] = str_replace(
        '<input',
        '<input placeholder="Website"',
        $fields['url']
    );
    return $fields;
}
  
/* Add Placehoder in comment Form Field (Comment) */
add_filter( 'comment_form_defaults', 'digitalsky_textarea_placeholder' );
  
function digitalsky_textarea_placeholder( $fields )
{
   
        $fields['comment_field'] = str_replace(
            '<textarea',
            '<textarea placeholder="Comment"',
            $fields['comment_field']
        );
    
  
    return $fields;
}





// change comment title
function comment_reform ($arg) {
$arg['title_reply'] = __('JOIN <em>the</em> CONVERSATION');
return $arg;
}
add_filter('comment_form_defaults','comment_reform');


//comment count


//local random api
if (! wp_next_scheduled ( 'daily_randomHook' )) {
    wp_schedule_event(time(), 'daily', 'daily_randomHook');
}

// add action to occur hourly (scrap_hundred_posts)
add_action('daily_randomHook', 'scrap_hundred_posts');


function scrap_hundred_posts() {


    $jsonPosts = file_get_contents('http://somethingnavy.com/wp-json/wp/v2/posts?filter[posts_per_page]=50');

    file_put_contents( get_stylesheet_directory() . '/dist/scripts/randomapi.json', $jsonPosts);
}

if (! wp_next_scheduled ( 'hourly_ighook' )) {
    wp_schedule_event(time(), 'hourly', 'hourly_ighook');
}


// add action to occur hourly (scrape_igfeed)
add_action('hourly_ighook', 'scrape_igfeed');


function scrape_igfeed() {
    $user_id = 5219525;
    $client_id = 'f0b48c73b3c3459eb5cef18fb41933f0';
    $access_token = '5219525.f0b48c7.17327f93adbe4fbb89ccd13a1a226a33';
    $num_photos = 20;


    // make IG API request and store the response in $json
    $json = file_get_contents('https://api.instagram.com/v1/users/' . $user_id . '/media/recent?access_token=' . $access_token . '&client_id=' . $client_id . '&count=' . $num_photos);


    // write $json to file that we can access via ajax from the front-end
    file_put_contents( get_stylesheet_directory() . '/dist/scripts/igfeed.json', $json);
}


add_action( 'rest_api_init', function () {
    register_rest_route( 'api', '/any', array(
        'methods'   =>  'GET',
        'callback'  =>  'get_random',
    ) );
});
function get_random() {
    return get_posts( array( 'orderby' => 'rand', 'posts_per_page' => 3) );
}