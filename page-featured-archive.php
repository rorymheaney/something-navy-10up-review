<?php get_template_part('templates/page', 'header'); ?>
<div class="archive-block">
	<span class="archive-block__alert">
		<?php echo esc_html( 'Please Select A Year First' ); ?>
	</span>
	<ul class="archive-block__list archive-block__list--year">

		<?php
			$years = $wpdb->get_col("SELECT DISTINCT YEAR(post_date) FROM $wpdb->posts ORDER BY post_date");
			foreach($years as $year) : 
		?>
			<li class="archive-block__item ">
				<span class="archive-block__selector archive-block__selector--year <?php if (date('Y') == $year):?>archive-block__selector--current-year<?php endif; ?>" data-year="<?php echo $year; ?>">
					<?php echo $year; ?>
				</span>
			</li>
		<?php endforeach; ?>

	</ul>

	<ul class="archive-block__list archive-block__list--month">
		<?php 
			$months = array(
				1 => 'Jan', 
				2 => 'Feb', 
				3 => 'Mar', 
				4 => 'Apr', 
				5 => 'May', 
				6 => 'Jun', 
				7 => 'Jul', 
				8 => 'Aug', 
				9 => 'Sep', 
				10 => 'Oct', 
				11 => 'Nov', 
				12 => 'Dec');
	$transposed = array_slice($months, date('n'), 12, true) + array_slice($months, 0, date('n'), true);
	$last8 = array_reverse(array_slice($transposed, -8, 12, true), true);
	foreach ($months as $num => $name)  :?>
	            <!-- printf('<option value="%u">%s</option>', $num, $name); -->
	        <li class="archive-block__item">
	        	<span class="archive-block__selector archive-block__selector--month <?php if (date('n') == $num):?>archive-block__selector--current-month<?php endif; ?>" data-month="<?php echo $num;?>">
	        		<?php echo $name;?>
	        	</span>
	        </li>
		<?php endforeach; ?>
	</ul>
</div>


<div class="archive-block__empty-con">
	<span class="archive-block__empty">
		<?php echo esc_html( 'That combination is empty! Please select a different combination' ); ?>
	</span>
</div>

<div class="archive-block archive-block--content">
	

	<?php $loop = new WP_Query( 
            array( 
                'post_type' => 'post',
                'posts_per_page' => 9
                 
                ) 
            ); 
    ?>
    <?php while ( $loop->have_posts() ) : $loop->the_post();

        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $image = $image[0];
    ?>

        <div class="medium-4 columns">
        	<div class="small-post__con">
	            <div class="small-post__image cover" style="background-image: url(<?php echo catch_that_image();?>)">
	                <a class="opacity-href" href="<?php the_permalink();?>" title="<?php the_title();?>"></a>
	            </div>
	            <a href="<?php the_permalink();?>" class="content-overlay content-overlay--padding text-styles">
	                <span class="content-overlay__background">
	                    
	                    <span class="content-overlay__position">
		                    <?php
							$category = get_the_category();
							if ($category) {
							  // echo '<a class="small-post__cat font__details font__details--bold" href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
								echo '<span class="small-post__cat font__details font__details--bold">'. $category[0]->name.'</span>';
							}
							?>

		                    <span class="content-overlay__title font__mini-header font__mini-header--other">
		                        <?php the_title();?>
		                    </span>
		                </span>

	                </span>
	            </a>
	        </div>
        </div>

     <?php endwhile; wp_reset_query();?>
</div>