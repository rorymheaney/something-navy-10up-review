
<?php if(is_home()):?>
	<?php //include('partials/home/video.php');?>

	<?php include('partials/home/slides.php');?>
<?php endif;?>

<div id="data-parameters" data-per_page="3" <?php if(is_category()):?>data-categories="<?php echo get_query_var('cat'); ?>"<?php endif;?> <?php if(is_tag()):?>data-tags="<?php $tag = get_queried_object(); echo $tag->term_id; ?>"<?php endif;?> data-page="2"></div>


<?php get_template_part('templates/page', 'header'); ?>

<div id="article-container">
	<?php if (!have_posts()) : ?>
		<div class="alert alert-warning">
			<?php _e('Sorry, no results were found.', 'sage'); ?>
		</div>
		<?php get_search_form(); ?>
	<?php endif; ?>

	<?php while (have_posts()) : the_post(); ?>
		<?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	<?php endwhile; ?>
</div>

<div id="load-more-button" class="load-more-posts load-more-posts--home">
	<span class="font__details font__details--bold" data-page="1">
		View More
	</span>
</div>
