var LoadArchivePosts = (function ($) {

	var archYear,
		archMonth,
		ajaxURL = $('body').data('ajaxurl');
        mainURL = ajaxURL + '/wp-json/wp/v2/posts';

	var activeYear = 'archive-block__selector--year-active',
		checkYear = '.archive-block__selector--year-active',
		$yearSelector = $('.archive-block__selector--year'),
		activeMonth = 'archive-block__selector--month-active',
		$monthSelector = $('.archive-block__selector--month'),
		$alertMessage = $('.archive-block__alert'),
		$emptyMessage = $('.archive-block__empty');

    function init() {

    	

    	//$('.archive-block__selector--current-month').delay(500).trigger('click');

    	$yearSelector.on('click',function(){
    		archYear = $(this).data('year');
    		$yearSelector.removeClass(activeYear);
    		$(this).addClass(activeYear);
    	});

    	

    	$monthSelector.on('click', function () {
    		
    		//check if there is an active year
    		if($(checkYear).length){

    			$alertMessage.fadeOut();
    			$emptyMessage.fadeOut();

    			$monthSelector.removeClass(activeMonth);
    			$(this).addClass(activeMonth);

    			// set month var
    			archMonth = $(this).data('month');

    			//set data being sent over
    			var dataSent = {
	    			filter: {
	    				'year' : archYear,
	    				'monthnum' : archMonth,
	    				'posts_per_page' : 9
	    			}
	    		};


	    		// start get request
		    		$.get( mainURL, dataSent, function( data ) {

			    		//console.log(data);
			    		//check to see if data is there!
			    		if (data.length !== 0) {

			    			//content variable
			    			var postPreview = "";



			    			$.each(data, function(a, postType){

			    				//first image url
			    				var firstContentImage = postType.first_image;

			    				var imageConfiguration = "";

			    				//no acf images, get first image
			    				if(postType.acf === false) {
			    					imageConfiguration +='<div class="small-post__image cover" style="background-image: url('+firstContentImage+')">' +
					    									'<a class="opacity-href" href="'+ postType.link +'" title="'+ postType.title.rendered +'"></a>'+
					    								'</div>';
								//using just one? 
			    				} else if(postType.acf.are_you_using_a_single_image_or_two === 'One') {
			    					imageConfiguration += '<div class="small-post__image cover" style="background-image: url('+postType.acf.singular_image_post.sizes.medium_large+')">' +
					    									'<a class="opacity-href" href="'+ postType.link +'" title="'+ postType.title.rendered +'"></a>'+
					    								'</div>';
								//using two
			    				} else if(postType.acf.are_you_using_a_single_image_or_two === 'Two') {
			    					imageConfiguration += '<a class="small-post__image-double clearfix" href="'+ postType.link +'" title="'+ postType.title.rendered +'">' +
			    											'<span class="small-post__double-left" style="background-image: url('+postType.acf.double_image_one.sizes.medium+')"></span>'+
			    											'<span class="small-post__double-right" style="background-image: url('+postType.acf.double_image_two.sizes.medium+')"></span>'+
				    									'</a>';
			    				}

			    				//set post Preview
			    				postPreview += '<div class="medium-4 columns">'+
				    								'<div class="small-post__con">'+
					    									imageConfiguration +
					    								'<a href="'+postType.link+'" class="content-overlay content-overlay--padding text-styles">'+
					    									'<span class="content-overlay__background">'+
					    										'<span class="content-overlay__position">'+
						    										'<span class="small-post__cat font__details font__details--bold">'+
						    											postType.pure_taxonomies.categories[0].name +
						    										'</span>'+
						    										'<span class="content-overlay__title font__mini-header font__mini-header--other">'+
												                        postType.title.rendered +
												                    '</span>'+
											                    '</span>'+
					    									'</span>'+
					    								'</a>'+
				    								'</div>'+
			    								'</div>';



			    			});

							$('.archive-block--content').fadeOut(function(){
								$(this).empty().append(postPreview).fadeIn();
							});



		    			//that didn't have anything??
			    		} else {

			    			$emptyMessage.fadeIn();

			    		}
			    		
			    		
					}, "json");
				//end get request

	    	//no active year? show alert	
    		} else {

    			//show the alert for god sakes!
    			$alertMessage.fadeIn();

    		}
    		

	    	

    	});
    	
    }


    


    return {
        init: init
    };
    

})(jQuery);