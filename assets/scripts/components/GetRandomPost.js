var GetRandomPost = (function ($) {

	var getBaseURL = $('body').data('ajaxurl');
        mainURL = getBaseURL + '/wp-content/themes/FancySquaresSomethingNavy/dist/scripts/randomapi.json';

    function init() {

    	$.get( ajaxURL, function( data ) {

    		function getRandomEntry() {
				return data[Math.round(Math.random() * (data.length - 1))];
			}

				//console.log(data);

			var markUp = '';
			for (var i=0; i<3; i++) {
				var entry = getRandomEntry();
				//console.log(entry.title.rendered);

				var title = entry.title.rendered,
					url = entry.link,
					img = entry.first_image,
					category = entry.pure_taxonomies.categories[0].cat_name;

				markUp += '<div class="medium-4 columns">'+
								'<div class="small-post__con">'+
									'<div class="small-post__image cover" style="background-image: url('+img+')">'+
										'<a class="opacity-href" href="'+ url +'" title="'+ title +'"></a>'+
									'</div>'+
									'<a href="'+ url +'" class="content-overlay content-overlay--padding text-styles">'+
										'<span class="content-overlay__background">'+
											'<span class="content-overlay__position">'+

												'<span class="small-post__cat font__details font__details--bold">'+category+'</span>'+
												
												'<span class="content-overlay__title font__mini-header">'+
										            title +
										        '</span>'+

											'</span>'+
											
										'</span>'+
									'</a>'+
								'</div>'+
						  '</div>';
			}

			$('.bubblingG').fadeOut();

			$('#random-fun-time').append(markUp);


    	});
    }

    


    return {
        init: init
    };
    

})(jQuery);