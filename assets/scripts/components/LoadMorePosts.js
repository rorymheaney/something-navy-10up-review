var LoadMorePosts = (function ($) {

	var ajaxURL = $('body').data('ajaxurl');
        mainURL = ajaxURL + '/wp-json/wp/v2/posts';


    function getYourFlexOn(){
        var $featuredSlider;

    
        function start() {
            $('body').removeClass('loading');
        }

        function beforeSlide(slider) {

        }

        $featuredSlider = $('.flexslider--home').flexslider({
            animation: "slide",
            animationSpeed: 650,
            // slideshow: false,
            directionNav: true,
            controlNav: false,
            move: 1,
            // manualControls: ".flex-control-nav li",
            // customDirectionNav: $(".featuredSlider .custom-navigation a"),
            mousewheel: false,
            animationLoop: true,
            start: start,
            itemWidth: 250,
            itemMargin: 5,
            before : beforeSlide,
            after: function (slider) {
                // Nothing Right Now
            },
            end: function (slider) {
                // Nothing Right Now
            }
        });
    }

    function init() {



        $loadBtn = $('#load-more-button span');

        $loadBtn.on('click', function () {
            var currentPage = $('#data-parameters').data('page');
            var nextPage = currentPage + 1;
            //console.log(nextPage);
            //if blog page
            var dataSent = [];
            $("#data-parameters").each(function(){
                var item = {};
                for(var i in $(this).data()){
                    item[i] = $(this).data(i);
                }

                console.log(item);
                dataSent.push(item);

            });

            //console.log(dataSent[0]);

            $.get( mainURL, dataSent[0], function( data ) {

                //console.log(data);
                
                if (data.length !== 0) {

                    var postPreview = "";

                    $.each(data, function(a, postType){

                        //first image url
                        var firstContentImage = postType.first_image,
                            postDate = moment(postType.date).format("MMMM D, YYYY");

                        var imageConfiguration = "";

                        //no acf images, get first image
                        if(postType.acf === false || postType.acf.are_you_using_a_single_image_or_two === '') {
                            imageConfiguration +='<div class="preview-image__placeholder" data-bgimage="'+firstContentImage+'" style="background-image: url('+firstContentImage+'"></div>';

                        //using just one? 
                        } else if(postType.acf.are_you_using_a_single_image_or_two === 'One') {
                            imageConfiguration += '<div class="preview-image__single-img"><img src="'+postType.acf.singular_image_post.url+'" alt="image"></div>';


                        //using two
                        } else if(postType.acf.are_you_using_a_single_image_or_two === 'Two') {
                            imageConfiguration += '<div class="preview-image__double-container">'+
                                                        '<div class="small-6">'+
                                                            '<img src="'+postType.acf.double_image_one.url+'" alt="image">'+
                                                        '</div>'+
                                                        '<div class="small-6">'+
                                                            '<img src="'+postType.acf.double_image_two.url+'" alt="image">'+
                                                        '</div>'+
                                                    '</div>';
                        }

                        //loop through categories 
                            var postTypeCategories = "";
                            if (undefined !== postType.pure_taxonomies.categories && postType.pure_taxonomies.categories.length) {
                                $.each(postType.pure_taxonomies.categories, function (b, postCategory) {
                                    

                                    postTypeCategories +='<li class="categories-list__category">'+
                                                            '<a class="categories-list__href font__sub-head" href="/category/'+postCategory.slug+'" title="View all posts in '+postCategory.cat_name+'">'+
                                                                postCategory.cat_name +
                                                            '</a>' +
                                                        '</li>';
                                });
                            }
                        //end loop

                        //loop through tags
                            var postTypeTags = "";
                            if (undefined !== postType.pure_taxonomies.tags && postType.pure_taxonomies.tags.length) {
                                $.each(postType.pure_taxonomies.tags, function (c, postTag) {

                                    postTypeTags += '<li>'+
                                                        '<a href="/tag/'+postTag.slug+'" title="View all posts in '+postTag.name+'">'+
                                                            postTag.name +
                                                        '</a>' +
                                                    '</li>';
                                });
                            }
                        //end loop

                        //crazy share mark up
                        var shareHtml = '';
                        var newTitle;

                        if(undefined !== postType.acf.title_unique && postType.acf.title_unique.length){
                            refinedTitle = postType.acf.title_unique;
                            regexedTitle = refinedTitle.replace(/<\/?p[^>]*>/g, "");
                            //console.log(regexedTitle);
                            newTitle = regexedTitle;
                        } else {
                            newTitle = postType.title.rendered;
                        }

                        //flexslider
                        var sliderMarkUP = '';
                        if(postType.acf.shopping_items_slider !== false ) {
                            
                            $.each(postType.acf.shopping_items_slider , function (f, flexSlide) {

                                    sliderMarkUP += '<li>'+
                                                '<a href="'+flexSlide.link+'" target="_blank">'+
                                                    '<img src="'+flexSlide.item.url+'" alt="accessory">'+
                                                    '<span class="font__shop-now">'+
                                                        'Shop Now'+
                                                    '</span>'+
                                                '</a>'+
                                            '</li>';
                                });
                        }

                        //set post Preview
                        postPreview += '<article class="content-preview">' +
                                        '<div class="preview-image preview-image--post">'+
                                            imageConfiguration + 
                                        '</div>'+
                                        //summary
                                        '<div class="content-preview__entry-summary">'+
                                            //summary con
                                            '<div class="content-preview__summary-con">'+
                                                '<div class="content-preview__meta">'+
                                                    '<ul class="categories-list categories-list--content-preview">'+
                                                        postTypeCategories +
                                                    '</ul>'+
                                                    '<time class="font__sub-head updated" datetime="'+postDate+'">'+postDate+'</time>'+
                                                '</div>'+
                                                '<h2 class="content-preview__entry-title">'+
                                                    '<a class="font__header" href="'+ postType.link +'">'+
                                                        newTitle +
                                                    '</a>'+
                                                '</h2>'+
                                                '<div id="featured-items-slider" class="flexslider flexslider--home '+(sliderMarkUP === '' ?'hide': '')+'">'+
                                                    '<ul class="slides">'+
                                                        sliderMarkUP +
                                                    '</ul>'+
                                                '</div>'+

                                            '</div>'+
                                            //end summary con
                                            '<a class="font__view-more" href="'+ postType.link +'">'+
                                                'View More'+
                                            '</a>'+
                                        '</div>'+
                                        //end summary
                                        //meta plus
                                        '<div class="content-preview__meta-plus clearfix">'+
                                            //comments
                                            '<div class="content-preview__comments">'+
                                                '<span class="font__details">'+
                                                    postType.fancy_comments + ' Comments' +
                                                '</span>'+
                                            '</div>'+
                                            //share
                                            '<div class="content-preview__share">'+
                                                '<span class="font__details">Share</span>'+
                                                '<div class="share-js">'+
                                                    '<div class="share-js__container">'+
                                                        '<ul>'+
                                                        //facebook
                                                            '<li>'+
                                                                '<a class="sbg-button sbg-button-facebook"  data-sbg-network="facebook"  data-sbg-url="'+ postType.link +'"  data-sbg-title="'+ postType.title.rendered +'"  data-sbg-summary=""  data-sbg-image="'+firstContentImage+'"  data-sbg-width="600"  data-sbg-height="368" >'+
  '<i class="sbg-button-icon fa fa-facebook"></i>'+
'</a>'+
                                                            '</li>'+
                                                              //twitter  
                                                            '<li>'+
                                                            '<a class="sbg-button sbg-button-twitter"  data-sbg-network="twitter"  data-sbg-text="'+ postType.link +'"  data-sbg-via="'+ postType.title.rendered +'"  data-sbg-hashtags=""  data-sbg-width="600"  data-sbg-height="258">'+
  '<i class="sbg-button-icon fa fa-twitter"></i>'+
'</a>'+
                                                            '</li>'+
                                                            //pin
                                                            '<li>'+
                                                                '<a class="sbg-button sbg-button-pinterest"  data-sbg-network="pinterest"  data-sbg-url="'+ postType.link +'"  data-sbg-media="'+firstContentImage+'"  data-sbg-description="'+ postType.title.rendered +'"  data-sbg-width="750"  data-sbg-height="322" >'+
  '<i class="sbg-button-icon fa fa-pinterest-p"></i>'+
'</a>'+
                                                            '</li>'+
                                                            //google
                                                            '<li>'+
                                                            '<a class="sbg-button sbg-button-google-plus"  data-sbg-network="google-plus"  data-sbg-url="'+ postType.link +'"  data-sbg-width="500"  data-sbg-height="505" >'+
  '<i class="sbg-button-icon fa fa-google-plus"></i>'+
'</a>'+
                                                            '</li>'+
                                                            //email
                                                            '<li>'+
                                                            '<a class="sbg-button sbg-button-email"  data-sbg-network="email"  data-sbg-subject="'+ postType.title.rendered +'"  data-sbg-body="'+ postType.link +'" >'+
  '<i class="sbg-button-icon fa fa-envelope-o"></i>'+
'</a>'+
                                                            '</li>'+
                                                            '<li>'+
                                                            '<a class="sbg-button sbg-button--twitter" href="http://www.tumblr.com/share/link?url='+ postType.link +'" class="ssbp-btn ssbp-tumblr" data-ssbp-title="'+ postType.title.rendered +'" data-ssbp-url="'+ postType.link +'" data-ssbp-site="Tumblr">'+
                                                                '<i class="sbg-button-icon fa fa-tumblr"></i>'+
                                                            '</a>'+
                                                            '</li>'+
                                                        '</ul>'+
                                                    '</div>'+
                                                '</div>'+
                                            '</div>' +
                                            //tags
                                            '<div class="content-preview__tags '+(postTypeTags === '' ?'hide': '')+'">'+
                                                '<span class="font__details">Tags:</span>'+
                                                '<ul class="tags-list tags-list--content-preview">'+
                                                    postTypeTags +
                                                '</ul>'+
                                            '</div>'+
                                        '</div>'+
                                        //end meta plus
                                    '</article>';


                            
                    });
                    
                    $('#article-container').append(postPreview);
                    
                    $('#data-parameters').data('page', nextPage);

                    //empty that bad boy
                    item = {};

                    ButtonShare.init();

                    getYourFlexOn();
                } else {
                    $('#load-more-button span').html('That\'s all for now');
                    setTimeout(function(){ 
                        $('#load-more-button').fadeOut();
                    }, 2000);

                }

                
                
            }, "json");

        });
        
    }



    return {
        init: init
    };
    

})(jQuery);