var ShoppingFlexSlider = (function ($) {
    var $featuredSlider;

    
    function start() {
        $('body').removeClass('loading');
    }

    function beforeSlide(slider) {

    }

    function init() {

        $featuredSlider = $('.flexslider--home').flexslider({
            animation: "slide",
            animationSpeed: 650,
            // slideshow: false,
            directionNav: true,
            controlNav: false,
            move: 1,
            // manualControls: ".flex-control-nav li",
            // customDirectionNav: $(".featuredSlider .custom-navigation a"),
            mousewheel: false,
            animationLoop: true,
            start: start,
            itemWidth: 250,
    		itemMargin: 5,
            before : beforeSlide,
            after: function (slider) {
                // Nothing Right Now
            },
            end: function (slider) {
                // Nothing Right Now
            }
        });
    }

    


    return {
        init: init
    };
})(jQuery);