var ShopStyle = (function ($) {

	


    function init() {
	// $.when( ajax1 , ajax2  ).done(function( a1, a2 ) {
		// a1 and a2 are arguments resolved for the page1 and page2 ajax requests, respectively.
		// Each argument is an array with the following structure: [ data, statusText, jqXHR ]
	//    var data = a1[ 0 ] + a2[ 0 ]; // a1[ 0 ] = "Whip", a2[ 0 ] = " It"
	//    if ( /Whip It/.test( data ) ) {
	//       alert( "We got what we came for!" );
	//    }
	// });
		
		var $grid = $('.shopstyle-con-list').imagesLoaded( function() {
			  // init Isotope after all images have loaded
			  $grid.isotope({
			    // options...
			    itemSelector: '.shopstyle-item'
			  });
			});


			// filter functions

			var filterFns = {
			  // show if number is greater than 50
			  numberGreaterThan50: function() {
			    var number = $(this).find('.number').text();
			    return parseInt( number, 10 ) > 50;
			  },
			  // show if name ends with -ium
			  ium: function() {
			    var name = $(this).find('.name').text();
			    return name.match( /ium$/ );
			  }
			};
			// bind filter button click
			$('.filters-button-group').on( 'click', 'button', function() {
			  var filterValue = $( this ).attr('data-filter');
			  // use filterFn if matches value
			  filterValue = filterFns[ filterValue ] || filterValue;
			  $grid.isotope({ filter: filterValue });
			});
			// change is-checked class on buttons
			$('.button-group').each( function( i, buttonGroup ) {
			  var $buttonGroup = $( buttonGroup );
			  $buttonGroup.on( 'click', 'button', function() {
			    $buttonGroup.find('.is-checked').removeClass('is-checked');
			    $( this ).addClass('is-checked');
			  });
			});

  //   	$.when(

  //   		//apparel
	 //    	$.get( "http://api.shopstyle.com/api/v2/lists/47077387/items?pid=uid6041-27578179-93&limit=20", function( data ) {
	 //    		//console.log(data);
	 //    		var elementHtml = "";
	 //    		$.each(data.favorites, function(a, apparel){
	 //    			// console.log(apparel.productId)

	 //    			elementHtml += '<div class="shopstyle-item shopstyle-item--apparel">'+
	 //    							'<div class="shopstyle-item__inner">'+
	 //    								'<a href="'+apparel.product.clickUrl+'" target="_blank">'+
	 //    									'<img src="'+apparel.product.image.sizes.IPhone.url+'" alt="shopstyle apparel"/>'+
  //   									'</a>'+
	 //    							'</div>'+
		// 						'</div>';
	 //    		});

	 //    		$('.shopstyle-con-list').append(elementHtml);


	 //    	}).fail(function() {
		// 		alert( "error" );
		// 	}),

		// 	//beauty
	 //    	$.get( "http://api.shopstyle.com/api/v2/lists/47077716/items?pid=uid6041-27578179-93&limit=20", function( data ) {
	 //    		//console.log(data);
	 //    		var elementHtml = "";
	 //    		$.each(data.favorites, function(b, beauty){
	 //    			// console.log(beauty.productId)

	 //    			elementHtml += '<div class="shopstyle-item shopstyle-item--beauty">'+
	 //    							'<div class="shopstyle-item__inner">'+
	 //    								'<a href="'+beauty.product.clickUrl+'" target="_blank">'+
	 //    									'<img src="'+beauty.product.image.sizes.IPhone.url+'" alt="shopstyle beauty"/>'+
  //   									'</a>'+
	 //    							'</div>'+
		// 						'</div>';
	 //    		});

	 //    		$('.shopstyle-con-list').append(elementHtml);


	 //    	}).fail(function() {
		// 		alert( "error" );
		// 	}),

		// 	//accessories
	 //    	$.get( "http://api.shopstyle.com/api/v2/lists/47077454/items?pid=uid6041-27578179-93&limit=20", function( data ) {
	 //    		//console.log(data);
	 //    		var elementHtml = "";
	 //    		$.each(data.favorites, function(c, accessories){
	 //    			// console.log(accessories.productId)

	 //    			elementHtml += '<div class="shopstyle-item shopstyle-item--accessories">'+
	 //    							'<div class="shopstyle-item__inner">'+
	 //    								'<a href="'+accessories.product.clickUrl+'" target="_blank">'+
	 //    									'<img src="'+accessories.product.image.sizes.IPhone.url+'" alt="shopstyle accessories"/>'+
  //   									'</a>'+
	 //    							'</div>'+
		// 						'</div>';
	 //    		});

	 //    		$('.shopstyle-con-list').append(elementHtml);


	 //    	}).fail(function() {
		// 		alert( "error" );
		// 	}),

		// 	//children
	 //    	$.get( "http://api.shopstyle.com/api/v2/lists/47077625/items?pid=uid6041-27578179-93&limit=20", function( data ) {
	 //    		//console.log(data);
	 //    		var elementHtml = "";
	 //    		$.each(data.favorites, function(d, child){
	 //    			// console.log(child.productId)

	 //    			elementHtml += '<div class="shopstyle-item shopstyle-item--child">'+
	 //    							'<div class="shopstyle-item__inner">'+
	 //    								'<a href="'+child.product.clickUrl+'" target="_blank">'+
	 //    									'<img src="'+child.product.image.sizes.IPhone.url+'" alt="shopstyle child"/>'+
  //   									'</a>'+
	 //    							'</div>'+
		// 						'</div>';
	 //    		});

	 //    		$('.shopstyle-con-list').append(elementHtml);
	 //    	}).fail(function() {
		// 		alert( "error" );
		// 	})

		// ).then(function() {

		// 	// All is ready now, so...

		// 	var $grid = $('.shopstyle-con-list').imagesLoaded( function() {
		// 	  // init Isotope after all images have loaded
		// 	  $grid.isotope({
		// 	    // options...
		// 	    itemSelector: '.shopstyle-item'
		// 	  });
		// 	});


		// 	// filter functions

		// 	var filterFns = {
		// 	  // show if number is greater than 50
		// 	  numberGreaterThan50: function() {
		// 	    var number = $(this).find('.number').text();
		// 	    return parseInt( number, 10 ) > 50;
		// 	  },
		// 	  // show if name ends with -ium
		// 	  ium: function() {
		// 	    var name = $(this).find('.name').text();
		// 	    return name.match( /ium$/ );
		// 	  }
		// 	};
		// 	// bind filter button click
		// 	$('.filters-button-group').on( 'click', 'button', function() {
		// 	  var filterValue = $( this ).attr('data-filter');
		// 	  // use filterFn if matches value
		// 	  filterValue = filterFns[ filterValue ] || filterValue;
		// 	  $grid.isotope({ filter: filterValue });
		// 	});
		// 	// change is-checked class on buttons
		// 	$('.button-group').each( function( i, buttonGroup ) {
		// 	  var $buttonGroup = $( buttonGroup );
		// 	  $buttonGroup.on( 'click', 'button', function() {
		// 	    $buttonGroup.find('.is-checked').removeClass('is-checked');
		// 	    $( this ).addClass('is-checked');
		// 	  });
		// 	});


		// });

			

    }




    


    return {
        init: init
    };
    

})(jQuery);