/* ========================================================================
* DOM-based Routing
* Based on http://goo.gl/EUTi53 by Paul Irish
*
* Only fires on body classes that match. If a body class contains a dash,
* replace the dash with an underscore when adding it to the object below.
*
* .noConflict()
* The routing is enclosed within an anonymous function so that you can
* always reference jQuery with $, even when in .noConflict() mode.
* ======================================================================== */

(function($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function() {
                // JavaScript to be fired on all pages

                $(document).foundation(); // Foundation JavaScript

                FixedNav.init();

                $('#menu-item-12037 a').on('click', function(event) {
                    event.preventDefault();
                    $('#contactModal').foundation('reveal','open');
                });

                
                if ($.cookie('modal_shown') == null) {
                    $.cookie('modal_shown', 'yes', { expires: 7, path: '/' });
                    $('#newsletterModal').foundation('reveal','open');
                }
                

                $(".site-header-bar__nav").on({
                    mouseenter: function () {
                        //stuff to do on mouse enter
                        $(this).addClass('site-header-bar__nav--plus');
                        $(this).find('.menu').fadeIn();
                    },
                    mouseleave: function () {
                        //stuff to do on mouse leave
                        $(this).removeClass('site-header-bar__nav--plus');
                        $(this).find('.menu').fadeOut();
                    }
                });

            },
            finalize: function() {
            // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function() {
                // JavaScript to be fired on the home page
                ShoppingFlexSlider.init();

                LoadMorePosts.init();

                PintItMagic.init();

                HomeMainSlider.init();
            },
            finalize: function() {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // single post.
        'single_post': {
            init: function() {
                // JavaScript to be fired on the single post pages
                GalleryPositionCheck.init();

                ShoppingFlexSlider.init();

                RecentlyPinned.init();

                PinItMagicSingle.init();

                GetRandomPost.init();

                $('li.comment').each(function(){
                    var newWidth = $(this).find('.comment-metadata').width();
                    //console.log(newWidth);
                    $(this).find('.reply').css('margin-left', newWidth + 35);
                });

                var isSmall;
                isSmall = function() {
                  return matchMedia(Foundation.media_queries['small']).matches && !matchMedia(Foundation.media_queries.medium).matches;
                };

                if (isSmall()) {
                    $('.side-bar-main').insertAfter('#after-content');
                }

                $('#commentform .comment-form-comment').insertAfter('#commentform .comment-form-url');

                

                if (!isSmall()) {
                    $(".side-bar-options").stick_in_parent({
                            parent: ".entry-content",
                            offset_top: 125,
                            recalc_every: 1
                        })
                      .on("sticky_kit:stick", function(e) {
                        //console.log("has stuck!", e.target);
                      })
                      .on("sticky_kit:unstick", function(e) {
                        //console.log("has unstuck!", e.target);
                      });

                      // console.log('new');    
                }
            }
        },
        // archive
        'page_id_801': {
            init: function() {
                // JavaScript to be fired on the archive pages
                LoadArchivePosts.init();
            }
        },
        // blog
        'blog': {
            init: function() {
                // JavaScript to be fired on the blog pages
            }
        },
        // natural archive
        'archive': {
            init: function() {
                // JavaScript to be fired on the natural archive pages
                ShoppingFlexSlider.init();
                
                PintItMagic.init();
            }
        },
        'category': {
            init: function() {
                // JavaScript to be fired on the category pages
                LoadMorePosts.init();
            }
        },
        // tag
        'tag': {
            init: function() {
                // JavaScript to be fired on the tags pages
                LoadMorePosts.init();
            }
        },
        // about
        'page_id_6861': {
            init: function() {
                // JavaScript to be fired on the about pages
                AboutPageCommentBox.init();
            }
        },
        // wish list
        'page_id_12046': {
            init: function() {
                // JavaScript to be fired on the wish list pages
                ShopStyle.init();
            }
        }
    };

// The routing fires all common scripts, followed by the page specific scripts.
// Add additional events for more control over timing e.g. a finalize event
var UTIL = {
    fire: function(func, funcname, args) {
        var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

        if (fire) {
            namespace[func][funcname](args);
        }
    },
    loadEvents: function() {
        // Fire common init JS
        UTIL.fire('common');

        // Fire page-specific init JS, and then finalize JS
        $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
            UTIL.fire(classnm);
            UTIL.fire(classnm, 'finalize');
        });

        // Fire common finalize JS
        UTIL.fire('common', 'finalize');
    }
};

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
