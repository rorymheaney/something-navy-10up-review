<!-- flexslider -->
<?php
// check if the repeater field has rows of data
if( have_rows('slides_home','option') ):?>
<div class="prevent-overflow prevent-overflow--slider-nav">
	<div id="home-page-slider" class="flexslider flexslider--home-featured">
		<ul class="slides">
		    <?php 
		    	while ( have_rows('slides_home','option') ) : the_row(); 
		    	$getSlide = get_sub_field('image');
		    ?>
		    	<li>
		    		<img src="<?php echo $getSlide['url'];?>" alt="accessory">
			    </li>
		    <?php endwhile; wp_reset_query();?>
		</ul>
	</div>
</div>
<?php else :?>

   

<?php endif;

?>