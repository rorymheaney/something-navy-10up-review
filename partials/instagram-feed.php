

<div class="instagram-container">


	<a href="https://www.instagram.com/somethingnavy/" class="instagram-tag" target="_blank">
	  <?php echo esc_html( '@somethingnavy' ); ?>
	</a>

	<?php	
		if(($instagramObject = get_transient("snavy_instagram")) === false) 
		{
			$userid = 5219525;
			$clientid = 'f0b48c73b3c3459eb5cef18fb41933f0';
			$num_photos = 20; 
			$instagramAPI = file_get_contents('https://api.instagram.com/v1/users/' . $userid . '/media/recent?access_token=5219525.f0b48c7.17327f93adbe4fbb89ccd13a1a226a33&client_id:'.$clientid.'&count:'.$num_photos);
			$instagramObject = json_decode( $instagramAPI , true );
			set_transient("snavy_instagram", $instagramObject, 3600);
		}
		
	// If we have instagramObject
	if ( false !== $instagramObject ) : ?>

	   	<?php //print_r($instagramObject['data']);?>

	   	<ul class="instagram-list">
		   <?php foreach( $instagramObject['data'] as $image ):?>

		   	<li class="instagram-list__item">
		   		<a href="<?php echo $image['images']['link'];?>" class="instagram-list__likes" target="_blank">
		   			<span class="wrap">
						<i class="fa fa-heart" aria-hidden="true"></i>
						<span class="count">
							<?php echo $image['likes']['count'];?>
						</span>
					</span>
		   		</a>
		   		<img src="<?php echo $image['images']['standard_resolution']['url'];?>">
		   	</li>

		   <?php endforeach; ?>
	   </ul>
	<?php endif;?>

</div>