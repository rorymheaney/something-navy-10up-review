<div id="newsletterModal" class="reveal-modal small" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<h2 class="font__mini-header font__mini-header--other">
		STAY UPDATED ON ALL THINGS SOMETHING NAVY
	</h2>

	<p class="short-block">
		<em>
			And get exclusive discounts from some of my favorite products through our newsletter!
		</em>
	</p>

	<?php echo gravity_form(3, false, false, false, '', true, 12);?>

	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>
