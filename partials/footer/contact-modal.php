<div id="contactModal" class="reveal-modal" data-reveal aria-labelledby="modalTitle" aria-hidden="true" role="dialog">
	<h2 class="font__mini-header font__mini-header--other">
		<?php the_field('title_contact_modal','option');?>
	</h2>
	<p class="font__details font__details--bold">
		<?php the_field('sub_title_contact_modal','option');?>
	</p>
	<p class="short-block">
		<em>
			<?php the_field('intro_contact_modal','option');?>
		</em>
	</p>

	<div class="reveal-modal__emails">
		<?php the_field('emails_contact_modal','option');?>
	</div>
	<p class="font__details font__details--bold title-ish">
		<?php the_field('tag_line_contact_modal','option');?>
	</p>
	<?php echo gravity_form(1, false, false, false, '', true, 12);?>
	<ul class="site-header-bar__social-media">
        <?php get_template_part('partials/social-links'); ?>
    </ul>
	<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div>