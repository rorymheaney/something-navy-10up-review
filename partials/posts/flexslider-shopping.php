<!-- flexslider -->
<?php
// check if the repeater field has rows of data
if( have_rows('shopping_items_slider') ):?>
<div id="featured-items-slider" class="flexslider flexslider--home">
	<ul class="slides">
	    <?php 
	    	while ( have_rows('shopping_items_slider') ) : the_row(); 
	    	$getSlide = get_sub_field('item');
	    ?>
	    	<li>
	    		<a href="<?php the_sub_field('link');?>" target="_blank">
		        	<img src="<?php echo $getSlide['url'];?>" alt="accessory">
		        	<span class="font__shop-now">
		        		Shop Now
		        	</span>
		        </a>
		    </li>
	    <?php endwhile; wp_reset_query();?>
	</ul>
</div>
<?php else :?>

   

<?php endif;

?>