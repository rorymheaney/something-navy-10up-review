<div id="prev-next-clicks" class="clearfix">

	
	
	<?php
    $next_post = get_next_post();
    if (!empty( $next_post )): 
        $nextThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id( $next_post->ID ), 'thumbnail' );
    ?>
    <div class="post-nav-events post-nav-events--next cover" style="background-image: url(<?php echo catch_next_post_image();?>);">
        <a class="post-nav-events__image-link" href="<?php echo get_permalink( $next_post->ID ); ?>">
            <?php //echo get_the_post_thumbnail($next_post->ID, 'full'); ?>
        </a>
        <a class="post-nav-events__text-link" href="<?php echo get_permalink( $next_post->ID ); ?>">
              
        </a>
        <span class="post-nav-events__text font__details"><?php echo esc_html( 'Previous Post' ); ?>
            </span>  
    </div>
    <?php endif; ?>

    

    <?php
    $prev_post = get_previous_post();
    if (!empty( $prev_post )): 
        $prevThumbURL = wp_get_attachment_image_src( get_post_thumbnail_id( $prev_post->ID ), 'thumbnail' );
    ?>
    <div class="post-nav-events post-nav-events--previous cover" style="background-image: url(<?php echo catch_prev_post_image();?>);">
        <a class="post-nav-events__image-link" href="<?php echo get_permalink( $prev_post->ID ); ?>">
            

        </a>
        <a class="post-nav-events__text-link" href="<?php echo get_permalink( $prev_post->ID ); ?>">
           
        </a>
		<span class="post-nav-events__text font__details"><?php echo esc_html( 'Next Post' ); ?>
            </span>

    </div>
    <?php endif; ?>


</div>