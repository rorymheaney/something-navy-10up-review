<div class="preview-image preview-image--single-post">
    
    <?php 
        //is there a singular crop?
        if(get_field('are_you_using_a_single_image_or_two') == "One") :
    ?>
        <div class="negative-margin">
            <?php $singleCrop = get_field('singular_image_post');?>
            <img src="<?php echo $singleCrop['url'];?>" alt="<?php echo $alt;?>">
        </div>
    <?php 
        //is there a double crop?
        elseif(get_field('are_you_using_a_single_image_or_two') == "Two"):
    ?>
    <?php 
        $doubleCropOne = get_field('double_image_one');
        $doubleCropTwo= get_field('double_image_two');
    ?>
        <div class="negative-margin">
            <div class="preview-image__double-container">
                <div class="small-6">
                    <img src="<?php echo $doubleCropOne['url'];?>" alt="<?php echo $alt;?>">
                </div>
                <div class="small-6">
                    <img src="<?php echo $doubleCropTwo['url'];?>" alt="<?php echo $alt;?>">
                </div>
            </div>
        </div>
    <?php 
        //featured image?
        elseif (has_post_thumbnail( $post->ID )):
    ?>
    <?php 
        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
        $image = $image[0];
    ?>
        <div class="negative-margin">
            <div class="preview-image__placeholder" style="background-image: url(<?php echo $image;?>);"></div>
        </div>
    <?php 
        //nothing left to get
        else:
    ?>
        <div class="preview-image__spacer"></div>
    <?php endif;?>

    
</div>


<?php while (have_posts()) : the_post(); ?>
    <div class="singular-post clearfix">
      

        <article <?php post_class(); ?>>
            <header>
                <div class="content-preview__meta">
                    <ul class="categories-list categories-list--content-preview">
                        <?php 
                            $sep = '';

                            foreach((get_the_category()) as $cat) {
                                echo $sep . '<li class="categories-list__category"><a class="categories-list__href font__sub-head" href="' . get_category_link($cat->term_id) . '"  title="View all posts in '. esc_attr($cat->name) . '">' . $cat->cat_name . '</a></li>';
                                // $sep = ', ';
                                $sep = '';
                            }
                        ?>
                    </ul>

                    <?php get_template_part('templates/entry-meta'); ?>
                </div>
                <h1 class="content-preview__entry-title entry-title font__header">
                    <?php if(get_field('title_unique')):?>
                        <?php the_field('title_unique', false, false);?>
                    <?php else:?>
                        <?php the_title(); ?>
                    <?php endif;?>
                </h1>
             
            </header>
            <div class="entry-content clearfix">

                <div class="entry-content__left">
                    <?php the_content(); ?>
                </div>

                <?php get_template_part('templates/sidebar'); ?>

            </div>



            <!-- slider -->
            <?php get_template_part('partials/posts/flexslider-shopping');?>

            <div id="after-content"></div>

        </article>
    </div>
      
    <!-- meta plus -->
    <?php get_template_part('partials/posts/meta-plus');?>
    <!-- meta plus end -->

<?php endwhile; ?>
<!-- previous and next -->
<?php get_template_part('partials/posts/prev-next');?>


<!-- random posts -->
<?php get_template_part('partials/posts/random-posts');?>
  
  <!-- comments -->
<?php comments_template('/templates/comments.php'); ?>


