<?php get_template_part('partials/footer/contact-modal'); // get contact modal?>


<?php get_template_part('partials/footer/newsletter-modal'); // get newsletter modal?>


<script data-blogname="SomethingNavy" id="cliqueCX" type="text/javascript" src="http://www.whowhatwear.com/clique-exchange/script.min.js"></script>

<?php get_template_part('partials/instagram-feed'); // get instagram feed?>


<footer class="content-info footer-content">
	<div class="instagram-feed">
	</div>

	<div class="footer-content__block clearfix">
		<div class="row row--plus">
			<!-- year copyright -->
			<div class="footer-content__space footer-content__year">
				<span>
					<?php echo esc_html( '©' ); ?> <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?>
				</span>
			</div>

			<!-- links -->
			<div class="footer-content__space footer-content__links footer-content__about">
				<a href="/about/">
					About
				</a>
			</div>

			<!-- links -->
			<div class="footer-content__space footer-content__links">
				<a href="#" data-reveal-id="contactModal">
					Contact
				</a>
			</div>

			<!-- site by -->
			<div class="footer-content__space footer-content__site-by">
				<a href="http://projectmplus.com/" target="_blank">
					<?php echo esc_html( 'Design + development by m plus' ); ?>
				</a>
			</div>

			<?php echo gravity_form(2, true, false, false, '', true, 12);?>
		</div>
	</div>
</footer>
