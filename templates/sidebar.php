<aside class="side-bar-main">
	<?php dynamic_sidebar('sidebar-primary'); ?>
	
	<div class="side-bar-options">
		
		<!-- favorite outfits -->
		<div class="side-bar-options__option side-bar-options__favorite-outfits">
			<h2 class="side-bar-options__title">
				<?php echo esc_html( 'My Favorite Outfits' ); ?>
			</h2>

			<?php 

			$posts = get_field('favorite_outfits_sidebar','option');

			if( $posts ): ?>
			    <ul class="my-favorites">
			    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
			        <?php setup_postdata($post); ?>
			        <li class="my-favorites__item">
			            <!-- <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			            <span>Custom field from $post: <?php the_field('author'); ?></span> -->
			            <?php 
						      //is there a singular crop?
						      if(get_field('are_you_using_a_single_image_or_two') == "One") :
						    ?>
						      <div class="negative-margin">
						      <?php $singleCrop = get_field('singular_image_post');?>
						      <img src="<?php echo $singleCrop['url'];?>" alt="<?php echo $alt;?>">
						    </div>
						    <?php 
						      //is there a double crop?
						      elseif(get_field('are_you_using_a_single_image_or_two') == "Two"):
						    ?>
						      <?php 
						        $doubleCropOne = get_field('double_image_one');
						        $doubleCropTwo= get_field('double_image_two');
						      ?>
						      <div class="negative-margin">
						      <div class="preview-image__double-container">
						        <div class="small-6">
						          <img src="<?php echo $doubleCropOne['url'];?>" alt="<?php echo $alt;?>">
						        </div>
						        <div class="small-6">
						          <img src="<?php echo $doubleCropTwo['url'];?>" alt="<?php echo $alt;?>">
						        </div>
						      </div>
						    </div>
						    <?php 
						      //featured image?
						      elseif (has_post_thumbnail( $post->ID )):
						    ?>
						      <?php 
						        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
						        $image = $image[0];
						      ?><div class="negative-margin">
						      <div class="preview-image__placeholder" style="background-image: url(<?php echo $image;?>);"></div>
						      </div>
						    <?php 
						      //nothing left to get
						      else:
						    ?>
						       <div class="preview-image__spacer"></div>
					    <?php endif;?>
					    <?php
							$category = get_the_category();
							if ($category) {
							  // echo '<a class="small-post__cat font__details font__details--bold" href="' . get_category_link( $category[0]->term_id ) . '" title="' . sprintf( __( "View all posts in %s" ), $category[0]->name ) . '" ' . '>' . $category[0]->name.'</a> ';
								echo '<span class="small-post__cat font__details font__details--bold">'. $category[0]->name.'</span>';
							}
							?>

		                    <a class="content-overlay__title font__mini-header font__mini-header--other">
		                        <?php the_title();?>
		                    </a>
			        </li>
			    <?php endforeach; ?>
			    </ul>
			    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
			<?php endif; ?>
			
			
		</div>

		<!-- pins -->
		<div class="side-bar-options__option side-bar-options__recent-pin">
			<h2 class="side-bar-options__title">
				<?php echo esc_html( 'RECENTLY PINNED' ); ?>
			</h2>

			<div id="load-pinterest">
			</div>
			
		</div>
		<!-- wish list -->
		<div class="side-bar-options__option side-bar-options__wish-list">
			<h2 class="side-bar-options__title">
				<?php echo esc_html( 'On My WishList' ); ?>
			</h2>
			<a class="sub-title font__body-copy font__body-copy--italic" href="<?php the_field('see_more_link','option');?>">
				<?php echo esc_html( 'See more of my finds >' ); ?>
			</a>
			<div class="wish-list-item">
				<?php $getWishListImage = get_field('wishlist_image_sidebar','option');?>
				<a href="<?php the_field('items_link_sidebar','option');?>" target="_blank">
					<img src="<?php echo $getWishListImage['url'];?>" alt="wishlist item">
				</a>
			</div>
		</div>

		<!-- form -->

		<div class="side-bar-options__option side-bar-options__form">
			<?php echo gravity_form(2, true, false, false, '', true, 30);?>
		</div>


	</div>

	<!-- social -->
<!-- 
	<ul class="site-header-bar__social-media site-header-bar__social-media--sidebar">
        <?php get_template_part('partials/social-links'); ?>
    </ul> -->
	
</aside>